import {Polygon, Point} from "wkx"

export class RectangularPolygon extends Polygon {
  constructor(min: Point, max: Point) {
    super([min, new Point(max.x, min.y), max, new Point(min.x, max.y), min])
  }
}
