import {createConnection, getConnection, getConnectionManager, Connection} from "typeorm"
import "reflect-metadata"

import {Feature} from "./entity"

async function createConnectionWrapper(): Promise<Connection> {
  return await createConnection({
    type: "mysql",
    url: process.env.DATABASE_URL,
    entities: [Feature],
    synchronize: true,
    legacySpatialSupport: false,
    ssl: true,
    extra: {
      poolSize: 10,
      keepConnectionAlive: true,
    },
  })
}

export {createConnectionWrapper as createConnection, getConnection, getConnectionManager}
export * from "./entity"
export * from "./enum"
export * from "./geom"
