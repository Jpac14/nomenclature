import {Column, Entity, PrimaryGeneratedColumn} from "typeorm"

import {Target} from "../enum"
import {Point, Polygon, Geometry} from "wkx"

interface FeatureInterface {
  id: number,
  name: string,
  target: Target,
  center: object,
  bounds: object,
}

@Entity()
export class Feature {
  @PrimaryGeneratedColumn("increment")
  id!: number

  @Column("text")
  name: string

  @Column({
    type: "enum",
    enum: Target,
  })
  target: Target

  @Column("point", {
    transformer: {
      from: (value: string): Point => {
        return Geometry.parse(value) as Point
      },
      to: (value: Point): string => {
        return value.toWkt()
      },
    },
  })
  center: Point

  @Column("polygon", {
    transformer: {
      from: (value: string): Polygon => {
        return Geometry.parse(value) as Polygon
      },
      to: (value: Polygon): string => {
        return value.toWkt()
      },
    },
  })
  bounds: Polygon

  asObject(): FeatureInterface {
    return {
      ...this,
      center: this.center.toGeoJSON(),
      bounds: this.bounds.toGeoJSON()
    }
  }

  constructor(name: string, target: Target, center: Point, bounds: Polygon) {
    this.name = name
    this.target = target
    this.center = center
    this.bounds = bounds
  }
}
