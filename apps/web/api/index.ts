import express from "express"

import {createConnection, getConnection} from "data"
import {Feature} from "data/entity"

async function obtainConnection() {
  try {
    return await getConnection()
  } catch (error) {
    return await createConnection()
  }
}

const app = express()

app.get("/api/geocode", async (req, res) => {
  const connection = await obtainConnection()
  const repository = await connection.getRepository(Feature)

  const {name, target, lat, lng} = req.query

  const query = await repository.createQueryBuilder()
  query.where("name LIKE :name", {name: `%${name}%`})
  if (target) query.where("target = :target", {target})
  if (lat && lng)
    query.orderBy(
      `ST_Distance(\`Feature\`.\`center\`, ST_GeomFromText('POINT(${lat} ${lng})'))`,
      "ASC"
    )

  const features = await query.getMany()
  return res.json(features.map((feature) => feature.asObject()))
})

app.get("/api/reverse_geocode", async (req, res) => {
  const connection = await obtainConnection()
  const repository = await connection.getRepository(Feature)

  const {lat, lng, target} = req.query

  const query = await repository.createQueryBuilder()
  query.where(`ST_Contains(\`Feature\`.\`bounds\`, ST_GeomFromText('POINT(${lat} ${lng})'))`)
  if (target) query.where("target = :target", {target})
  query.orderBy(
    `ST_Distance(\`Feature\`.\`center\`, ST_GeomFromText('POINT(${lat} ${lng})'))`,
    "ASC"
  )

  const features = await query.getMany()
  return res.json(features.map((feature) => feature.asObject()))
})

module.exports = app
