import {createConnection} from "data"
import {Feature} from "data/entity"
import {Target} from "data/enum"
import {RectangularPolygon} from "data/geom"

import {Point} from "wkx"

import {Cluster} from "puppeteer-cluster"
import {Page} from "puppeteer"

import axios from "axios"
import Papa from "papaparse"

const BASE_URL = "https://planetarynames.wr.usgs.gov/SearchResults?target="

interface FeatureInput {
  Feature_Name: string
  Target: string
  Center_Latitude: number
  Center_Longitude: number
  Northern_Latitude: number
  Southern_Latitude: number
  Eastern_Longitude: number
  Western_Longitude: number
}

async function changeInputValue(page: Page, selector: string, value: boolean) {
  await page.$eval(
    selector,
    (el, value) => {
      el.setAttribute("value", String(value))
      el.setAttribute("checked", "checked")
    },
    value
  )
}

async function main() {
  const connection = await createConnection()
  const repository = await connection.getRepository(Feature)

  const cluster = await Cluster.launch({
    concurrency: Cluster.CONCURRENCY_CONTEXT,
    maxConcurrency: 2,
    retryLimit: 3,
    retryDelay: 5000,
    monitor: true,
  })

  cluster.on("taskerror", (err, data) => {
    console.error(`Error crawling ${data}: ${err.message}`)
  })

  await cluster.task(async ({page, data: target}) => {
    // Intercept requests
    await page.setRequestInterception(true)
    page.on("request", async (request) => {
      // Check if it is the CSV request
      if (request.method() === "POST") {
        await request.abort()

        // Recreate request
        const response = await axios.post(request.url(), request.postData())

        // Parse data and write to data
        Papa.parse<FeatureInput>(response.data, {
          header: true,
          skipEmptyLines: true,
          dynamicTyping: true,
          complete: (result) => {
            repository.insert(
              result.data.map((row) => {
                return new Feature(
                  row.Feature_Name,
                  target,
                  new Point(row.Center_Latitude, row.Center_Longitude),
                  new RectangularPolygon(
                    new Point(row.Northern_Latitude, row.Eastern_Longitude),
                    new Point(row.Southern_Latitude, row.Western_Longitude)
                  )
                )
              })
            )
          },
        })

        return
      }
      await request.continue()
    })

    await page.goto(BASE_URL + target, {waitUntil: "networkidle0"})

    // Select columns
    await changeInputValue(page, "input[name='diameterColumn']", false) // Toggle off
    await changeInputValue(page, "input[name='latLonColumn']", true) // Toggle on
    await changeInputValue(page, "input[name='coordSystemColumn']", false) // Toggle off
    await changeInputValue(page, "input[name='approvalStatusColumn']", false) // Toggle off
    await changeInputValue(page, "input[name='approvalDateColumn']", false) // Toggle off
    await changeInputValue(page, "input[name='originColumn']", false) // Toggle off

    // CSS selectors of the CSV link
    await page.click(
      "#form > div:nth-child(35) > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1)"
    )
  })

  Object.keys(Target)
    .filter((target) => isNaN(parseInt(target)))
    .forEach((target) => cluster.queue(target))

  await cluster.idle()
  await cluster.close()
}

main().finally(() => process.exit(0))
